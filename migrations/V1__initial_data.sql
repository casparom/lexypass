CREATE TABLE payment_methods(id TEXT PRIMARY KEY, valid_until TEXT NOT NULL, security_number INTEGER NOT NULL);
