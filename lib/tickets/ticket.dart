import 'package:lexypass/tickets/tickets.dart';

class Ticket {
  final TicketType ticketType;
  final String description;
  final DateTime validUntil;

  Ticket(this.ticketType, this.description, this.validUntil);

  bool isValid() => validUntil.isAfter(DateTime.now());
}
