import 'package:google_maps_webservice/directions.dart';

import 'ticket_product.dart';

abstract class Merchant {
  bool supports(TransitDetails transitDetails);
  Future<List<TicketProduct>> getTicketOptions(TransitDetails transitDetails);
}
