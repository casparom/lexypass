import 'price.dart';

class TicketProduct {
  TicketType ticketType;
  TravelerType travelerType;
  String description;
  Price price;

  TicketProduct(
      this.ticketType, this.travelerType, this.description, this.price);
}

enum TicketType { SINGLE_TICKET, TRAVEL_CARD }

enum TravelerType { ADULT }
