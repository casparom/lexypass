import 'package:google_maps_webservice/directions.dart';
import 'package:lexypass/tickets/tickets.dart';

import 'merchant.dart';

class MerchantFacade {
  static final MerchantFacade _instance = MerchantFacade._privateConstructor();

  factory MerchantFacade() => _instance;

  MerchantFacade._privateConstructor();

  List<Merchant> _merchants = List<Merchant>();

  void add(Merchant merchant) {
    _merchants.add(merchant);
  }

  Future<List<TicketProduct>> getTicketProducts(
          TransitDetails transitDetails) =>
      _getFirstMerchant(transitDetails).getTicketOptions(transitDetails);

  Merchant _getFirstMerchant(TransitDetails transitDetails) =>
      _merchants.firstWhere((merchant) => merchant.supports(transitDetails));
}
