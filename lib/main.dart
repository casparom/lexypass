import 'package:flutter/material.dart';
import 'package:lexypass/localStorage/database.dart';
import 'package:lexypass/localStorage/database_factory.dart';
import 'package:lexypass/localStorage/payment_method_local_repository.dart';
import 'package:lexypass/merchant_registry.dart';
import 'package:lexypass/payment/payment_method_facade.dart';
import 'package:permission_handler/permission_handler.dart';

import 'ui/pages/startPage/start_page.dart';

void main() {
  MerchantRegistry().register();
  runApp(MyApp(LocalDatabaseFactory()));
}

class MyApp extends StatefulWidget {
  final LocalDatabaseFactory _databaseFactory;

  MyApp(this._databaseFactory);

  @override
  State createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  LocalDatabase _database;

  @override
  void initState() {
    widget._databaseFactory.create().then((database) {
      _database = database;
      PaymentMethodFacade()
          .setRepository(PaymentMethodLocalRepository(_database));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _getPermissions();
    return MaterialApp(
      title: 'Lexypass',
      theme: ThemeData(
        primarySwatch: createColorSwatch(),
      ),
      home: StartPage(),
    );
  }

  void _getPermissions() async {
    PermissionStatus status = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);
    if (status != PermissionStatus.granted) {
      await PermissionHandler().requestPermissions([PermissionGroup.location]);
    }
  }

  @override
  void dispose() {
    _database.close();
    super.dispose();
  }

  MaterialColor createColorSwatch() {
    return MaterialColor(
      Colors.black.value,
      <int, Color>{
        50: Colors.red,
        100: Colors.red,
        200: Colors.red,
        300: Colors.red,
        400: Colors.red,
        500: Colors.red,
        600: Colors.red,
        700: Colors.red,
        800: Colors.red,
        900: Colors.red,
      },
    );
  }
}
