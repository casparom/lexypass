import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'migration/migrator.dart';

class LocalDatabase {
  static final String _databaseName = "lexypass.db";
  static final int _version = 1;

  Database _db;

  static Future<LocalDatabase> initialize() {
    return _openDatabase().then((db) => LocalDatabase._privateConstructor(db));
  }

  LocalDatabase._privateConstructor(this._db);

  static Future<Database> _openDatabase() async {
    var databasesPath = await getDatabasesPath();
    await deleteDatabase(
        join(databasesPath, _databaseName)); // TODO REMOVE THIS
    var path = join(databasesPath, _databaseName);

    try {
      await Directory(databasesPath).create(recursive: true);
    } catch (_) {}
    return openDatabase(
      path,
      onCreate: _onCreate,
      version: _version,
    );
  }

  static void _onCreate(Database database, int version) async {
    await database.transaction((txn) async {
      (await Migrator().getMigrationCommands()).forEach((command) {
        txn.execute(command);
      });
    });
  }

  Future<void> close() => _db.close();

  Future<List<Map<String, dynamic>>> read(String table) => _db.query(table);

  void insert(String table, Map<String, dynamic> values) {
    _db.insert(
      table,
      values,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }
}
