import 'package:flutter/services.dart';

class Migrator {
  static String _migrationFile = "V1__initial_data.sql";

  Future<List<String>> getMigrationCommands() async {
    String contents = await _readMigrationFile(_migrationFile);
    return contents
        .split(";")
        .map((command) => command.trim())
        .where((command) => command != "")
        .toList();
  }

  Future<String> _readMigrationFile(String fileName) async =>
      await rootBundle.loadString('migrations/$fileName');
}
