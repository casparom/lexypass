import 'database.dart';

class LocalDatabaseFactory {
  Future<LocalDatabase> create() => LocalDatabase.initialize();
}
