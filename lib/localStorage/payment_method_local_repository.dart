import 'package:lexypass/localStorage/database.dart';
import 'package:lexypass/payment/methods/payment_method_repository.dart';
import 'package:lexypass/payment/payment.dart';

class PaymentMethodLocalRepository extends PaymentMethodRepository {
  LocalDatabase _database;
  final String _tableName = "payment_methods";

  PaymentMethodLocalRepository(this._database);

  @override
  Future<List<PaymentMethod>> getAll() async =>
      (await _database.read(_tableName))
          .map((map) => _mapToPaymentMethod(map))
          .toList();

  PaymentMethod _mapToPaymentMethod(Map<String, dynamic> map) =>
      CreditCard.fromMap(map);

  @override
  void saveCreditCard(CreditCard card) {
    _database.insert(_tableName, card.toMap());
  }
}
