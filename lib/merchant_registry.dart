import 'package:lexypass/ticketMerchants/test_merchant.dart';
import 'package:lexypass/tickets/merchant.dart';
import 'package:lexypass/tickets/merchant_facade.dart';

class MerchantRegistry {
  final List<Merchant> _merchants = [TestMerchant()];

  void register() {
    _merchants.forEach((merchant) => new MerchantFacade().add(merchant));
  }
}
