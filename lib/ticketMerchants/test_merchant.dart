import 'package:lexypass/tickets/tickets.dart';

class TestMerchant extends Merchant {
  bool supports(TransitDetails transitDetails) => true;

  Future<List<TicketProduct>> getTicketOptions(TransitDetails transitDetails) =>
      Future.delayed(
        Duration(seconds: 3),
        () => [
          TicketProduct(
            TicketType.SINGLE_TICKET,
            TravelerType.ADULT,
            "1 ride",
            Price(1, "\$"),
          ),
          TicketProduct(
            TicketType.TRAVEL_CARD,
            TravelerType.ADULT,
            "24 hours",
            Price(10, "\$"),
          ),
        ],
      );
}
