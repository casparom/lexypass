import 'package:flutter/material.dart';
import 'package:google_maps_webservice/directions.dart' as Directions;
import 'package:google_maps_webservice/places.dart';
import 'package:lexypass/global/properties.dart';

Directions.GoogleMapsDirections _directions =
    Directions.GoogleMapsDirections(apiKey: Google.apiKey);

Future<Directions.DirectionsResponse> getDirections(
        PlaceDetails origin, PlaceDetails destination) =>
    _directions.directions(
      "place_id:" + origin.placeId,
      "place_id:" + destination.placeId,
      travelMode: Directions.TravelMode.transit,
      alternatives: true,
    );

Icon chooseTravelModeIcon(Directions.Step step) {
  switch (step.travelMode) {
    case Directions.TravelMode.walking:
      {
        return Icon(Icons.directions_walk);
      }
      break;

    default:
      {
        return Icon(Icons.directions_transit);
      }
      break;
  }
}
