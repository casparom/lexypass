import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:lexypass/global/properties.dart';
import 'package:uri/uri.dart';

enum StaticMapViewType { roadmap, satellite, hybrid, terrain }

class StaticMapProvider {
  final String googleMapsApiKey = Google.apiKey;
  static const int defaultZoomLevel = 4;
  static const int defaultWidth = 600;
  static const int defaultHeight = 400;
  static const StaticMapViewType defaultMaptype = StaticMapViewType.roadmap;
  static const LatLng defaultCenter = LatLng(0, 0);

  Uri getStaticUriWithMarkersAndZoom({
    List<LatLng> markers,
    int width,
    int height,
    StaticMapViewType mapType,
    LatLng center,
    int zoomLevel,
    String pathPoints,
  }) {
    var finalUri = new UriBuilder()
      ..scheme = 'https'
      ..host = 'maps.googleapis.com'
      ..port = 443
      ..path = '/maps/api/staticmap';

    if (center == null && (markers == null || markers.length == 0)) {
      center = defaultCenter;
    }

    if (markers == null || markers.length == 0) {
      if (center == null) center = defaultCenter;
      finalUri.queryParameters = {
        'center': '${center.latitude},${center.longitude}',
        'zoom': zoomLevel.toString(),
        'size': '${width ?? defaultWidth}x${height ?? defaultHeight}',
        'maptype': _getMapTypeQueryParam(mapType),
        'key': googleMapsApiKey,
      };
    } else {
      List<String> markerPositions = markers.map((marker) {
        num lat = marker.latitude;
        num lng = marker.longitude;
        return '$lat,$lng';
      }).toList();

      String markersString = markerPositions.join('|');
      finalUri.queryParameters = {
        'markers': markersString,
        'size': '${width ?? defaultWidth}x${height ?? defaultHeight}',
        'maptype': _getMapTypeQueryParam(mapType),
        'key': googleMapsApiKey,
      };
    }
    if (center != null)
      finalUri.queryParameters['center'] =
          '${center.latitude},${center.longitude}';

    if (pathPoints != null)
      finalUri.queryParameters["path"] = "enc:" + pathPoints;

    var uri = finalUri.build();
    return uri;
  }

  String _getMapTypeQueryParam(StaticMapViewType maptype) {
    String mapTypeQueryParam;
    switch (maptype) {
      case StaticMapViewType.roadmap:
        mapTypeQueryParam = "roadmap";
        break;
      case StaticMapViewType.satellite:
        mapTypeQueryParam = "satellite";
        break;
      case StaticMapViewType.hybrid:
        mapTypeQueryParam = "hybrid";
        break;
      case StaticMapViewType.terrain:
        mapTypeQueryParam = "terrain";
        break;
    }
    return mapTypeQueryParam;
  }
}
