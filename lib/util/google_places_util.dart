import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:lexypass/global/properties.dart';

GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: Google.apiKey);

Future<PlaceDetails> getPlaceDetailsByPlaceId(String placeId) async =>
    (await _places.getDetailsByPlaceId(placeId)).result;

LatLng getLatLngByPlaceDetails(PlaceDetails details) =>
    LatLng(details.geometry.location.lat, details.geometry.location.lng);
