import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart' as Google;
import 'package:location/location.dart';
import 'package:tuple/tuple.dart';

export 'package:tuple/tuple.dart';

LatLng locationDataToLatLng(LocationData locationData) =>
    LatLng(locationData.latitude, locationData.longitude);

LatLng locationToLatLng(Google.Location location) =>
    LatLng(location.lat, location.lng);

Tuple2 calculateSouthWestAndNorthEast(LatLng one, LatLng two) {
  LatLng southwest;
  LatLng northEast;
  if (one.latitude <= two.latitude) {
    if (one.longitude <= two.longitude) {
      southwest = one;
      northEast = two;
    } else {
      southwest = LatLng(one.latitude, two.longitude);
      northEast = LatLng(two.latitude, one.longitude);
    }
  } else {
    if (one.longitude <= two.longitude) {
      southwest = LatLng(two.latitude, one.longitude);
      northEast = LatLng(one.latitude, two.longitude);
    } else {
      southwest = two;
      northEast = one;
    }
  }
  return Tuple2(southwest, northEast);
}
