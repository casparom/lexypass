import 'package:flutter/material.dart';
import 'package:lexypass/payment/payment.dart';
import 'package:lexypass/payment/payment_method_facade.dart';
import 'package:lexypass/payment/payment_service.dart';
import 'package:lexypass/tickets/ticket_product.dart';
import 'package:lexypass/ui/pages/paymentConfirmedPage/payment_confirmed_page.dart';
import 'package:lexypass/ui/shared/addCreditCardDialog/add_credit_card_dialog.dart';
import 'package:lexypass/ui/shared/single_button_footer.dart';

import 'bill/bill.dart';
import 'paymentMethodSelector/payment_method_selector.dart';

class PaymentPage extends StatefulWidget {
  final Map<TicketProduct, int> _productCounts;

  PaymentPage(this._productCounts);

  @override
  State createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  PaymentMethod _paymentMethod;
  bool _isWaitingForPayment = false;
  List<PaymentMethod> _paymentMethods = [];

  @override
  void initState() {
    PaymentMethodFacade().getMethods().then((methods) {
      setState(() {
        _paymentMethods = methods;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pay for tickets"),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Bill(widget._productCounts),
            PaymentMethodSelector(
                _paymentMethods, _paymentMethod, _onPaymentMethodChange),
            Divider(),
            InkWell(
              onTap: () => AddCreditCardDialog.show(context).then((card) {
                if (card != null) {
                  initState();
                }
              }),
              child: Text(
                "Add payment method",
                style: TextStyle(color: Colors.red),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: SingleButtonFooter(
        () => _onConfirmTap(context),
        _isWaitingForPayment ? Icon(Icons.call) : Text("CONFIRM PAYMENT"),
      ),
    );
  }

  void _onPaymentMethodChange(PaymentMethod method) {
    setState(() {
      _paymentMethod = method;
    });
  }

  void _onConfirmTap(BuildContext context) {
    setState(() {
      _isWaitingForPayment = true;
    });
    _pay(context);
  }

  void _pay(BuildContext context) {
    PaymentServiceFacade()
        .pay(widget._productCounts, _paymentMethod)
        .then((success) {
      setState(() {
        _isWaitingForPayment = false;
      });
      if (success) {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => PaymentConfirmedPage()));
      } else {
        showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: Text("Payment failed"),
                ));
      }
    });
  }
}
