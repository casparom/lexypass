import 'package:flutter/material.dart';
import 'package:lexypass/payment/payment.dart';

import 'payment_method_radio.dart';

class PaymentMethodSelector extends StatelessWidget {
  final List<PaymentMethod> _methods;
  final PaymentMethod _selection;
  final Function(PaymentMethod) _onChanged;

  PaymentMethodSelector(this._methods, this._selection, this._onChanged);

  @override
  Widget build(BuildContext context) => Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Payment methods"),
            Column(
              children: _methods
                  .map((method) => PaymentMethodRadio(
                      method, _onChanged, _selection ?? _methods.first))
                  .toList(),
            )
          ],
        ),
        color: Color(0xEEEEEEFF),
      );
}
