import 'package:flutter/material.dart';
import 'package:lexypass/payment/payment.dart';

class PaymentMethodRadio extends RadioListTile {
  PaymentMethodRadio(PaymentMethod method, Function(PaymentMethod) _onChanged,
      PaymentMethod _groupValue)
      : super(
          title: Row(
            children: [
              Icon(Icons.credit_card),
              SizedBox(
                width: 10,
              ),
              Text(method.name()),
            ],
          ),
          value: method,
          groupValue: _groupValue,
          onChanged: (value) => _onChanged(value as PaymentMethod),
          controlAffinity: ListTileControlAffinity.trailing,
        );
}
