import 'package:flutter/material.dart';
import 'package:lexypass/tickets/tickets.dart';

import 'product_list_tile.dart';

class ProductList extends StatelessWidget {
  final Map<TicketProduct, int> _productCounts;

  ProductList(this._productCounts);

  @override
  Widget build(BuildContext context) => Column(
        children: _productCounts.entries
            .map((entry) => ProductListTile(entry.key, entry.value))
            .toList(),
      );
}
