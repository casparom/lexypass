import 'package:flutter/material.dart';
import 'package:lexypass/tickets/tickets.dart';

import 'totals_row.dart';

class Totals extends StatelessWidget {
  final Map<TicketProduct, int> _productCounts;
  final int _vat;

  Totals(this._productCounts, this._vat);

  @override
  Widget build(BuildContext context) => Column(
        children: <Widget>[
          TotalsRow(
            "Subtotal",
            _calculateSubTotal().toStringAsFixed(2).toString() +
                " " +
                _currency(),
          ),
          TotalsRow(
            "VAT (${_vat.toString()}%)",
            _calculateVat().toStringAsFixed(2).toString() + " " + _currency(),
          ),
          TotalsRow(
            "Total",
            _calculateTotal().toStringAsFixed(2).toString() + " " + _currency(),
            isBold: true,
          ),
        ],
      );

  double _calculateSubTotal() => _calculateTotal() / 100 * (100 - _vat);

  double _calculateVat() => _calculateTotal() / 100 * _vat;

  double _calculateTotal() => _productCounts.entries
      .fold(0, (sum, entry) => sum += (entry.key.price.value * entry.value));

  String _currency() => _productCounts.keys.first.price.currency;
}
