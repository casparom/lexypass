import 'package:flutter/material.dart';
import 'package:lexypass/tickets/tickets.dart';

import 'product_list.dart';
import 'totals.dart';

class Bill extends StatelessWidget {
  final Map<TicketProduct, int> _productCounts;

  Bill(this._productCounts);

  @override
  Widget build(BuildContext context) => Column(
        children: <Widget>[
          ProductList(_productCounts),
          Divider(),
          Totals(_productCounts, 20),
        ],
      );
}
