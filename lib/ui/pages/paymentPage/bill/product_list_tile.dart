import 'package:flutter/material.dart';
import 'package:lexypass/tickets/tickets.dart';

class ProductListTile extends StatelessWidget {
  final TicketProduct _product;
  final int _count;

  ProductListTile(this._product, this._count);

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: <Widget>[
              Text(_product.ticketType.toString()),
              Text(_product.description),
            ],
          ),
          Icon(Icons.close),
          Text(_count.toString()),
          Text((_product.price.value * _count).toString() +
              " " +
              _product.price.currency),
        ],
      );
}
