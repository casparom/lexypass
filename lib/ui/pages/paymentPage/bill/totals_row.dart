import 'package:flutter/material.dart';

class TotalsRow extends StatelessWidget {
  final String _title;
  final String _trailing;
  final bool isBold;

  TotalsRow(this._title, this._trailing, {this.isBold = false});

  @override
  Widget build(BuildContext context) => Row(
        children: <Widget>[
          Text(_title,
              style: isBold ? TextStyle(fontWeight: FontWeight.bold) : null),
          Spacer(),
          Text(_trailing,
              style: isBold ? TextStyle(fontWeight: FontWeight.bold) : null)
        ],
      );
}
