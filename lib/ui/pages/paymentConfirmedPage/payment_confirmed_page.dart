import 'package:flutter/material.dart';
import 'package:lexypass/ui/pages/myPassesPage/my_passes_page.dart';

class PaymentConfirmedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        title: Center(
          child: Text("Payment confirmed"),
        ),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Spacer(
              flex: 2,
            ),
            Expanded(
              child: Icon(Icons.bookmark),
              flex: 4,
            ),
            Text(
              "Payment for your tickets\nwas successfully processed.",
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              maxLines: 2,
            ),
            Spacer(
              flex: 1,
            ),
            Expanded(
              child: OutlineButton(
                borderSide: BorderSide(
                  color: Colors.red,
                ),
                child: Text("PROCEED TO YOUR PASSES"),
                onPressed: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MyPassesPage())),
              ),
            ),
            Spacer(
              flex: 5,
            ),
          ],
        ),
      ),
    );
  }
}
