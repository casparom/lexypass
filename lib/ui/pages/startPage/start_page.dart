import "package:flutter/material.dart";
import "package:google_maps_flutter/google_maps_flutter.dart";
import 'package:google_maps_webservice/geolocation.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:lexypass/global/properties.dart';
import 'package:lexypass/ui/pages/routePlanningPage/route_planning_page.dart';
import 'package:lexypass/ui/shared/bottomNavigationBar/bottom_navigation_bar.dart';
import 'package:lexypass/ui/shared/map_widget.dart';
import 'package:lexypass/util/location_util.dart';
import 'package:location/location.dart' as LocationFetcher;

class StartPage extends StatefulWidget {
  @override
  _StartPageState createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  Marker marker;
  Location location;

  GoogleMapController _mapController;

  @override
  void initState() {
    _updateDeviceLocation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _updateDeviceLocation();

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          leading: Container(),
          title: Text("Where are you going?"),
        ),
        body: Column(
          children: <Widget>[
            TextField(
              readOnly: true,
              onTap: () {
                if (location != null) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RoutePlanningPage(location)));
                } else {
                  showDialog(
                      context: context,
                      child: Text("Your location is still unknown"));
                }
              },
              decoration:
                  InputDecoration(hintText: "Search place or addess..."),
            ),
            MapWidget(
              onMapCreated: (controller) {
                this._mapController = controller;
                _moveToDeviceLocation();
              },
            ),
          ],
        ),
        bottomNavigationBar: LexyBottomNavigationBar(0),
      ),
    );
  }

  Future<Location> _updateDeviceLocation() {
    return LocationFetcher.Location()
        .getLocation()
        .then((LocationFetcher.LocationData data) {
      Location newLocation = Location(data.latitude, data.longitude);
      setState(() {
        location = newLocation;
      });
      return Future<Location>.value(newLocation);
    });
  }

  void _moveToDeviceLocation() async {
    await _updateDeviceLocation();
    _moveToLocation(locationToLatLng(location));
  }

  void _moveToLocation(LatLng latLng) {
    _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: latLng, zoom: MapView.mapZoom),
      ),
    );
  }
}
