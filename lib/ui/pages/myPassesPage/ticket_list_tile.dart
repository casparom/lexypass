import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../tickets/ticket.dart';

class TicketListTile extends StatelessWidget {
  final Ticket _ticket;

  TicketListTile(this._ticket);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Icon(Icons.book),
            Column(
              children: <Widget>[
                Text(_ticket.ticketType.toString()),
                Text(_ticket.description),
              ],
            ),
            Spacer(),
            Text("Valid until " +
                DateFormat("dd.MM.yyyy").format(_ticket.validUntil)),
          ],
        ),
        Divider(),
      ],
    );
  }
}
