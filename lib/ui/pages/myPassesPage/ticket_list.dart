import 'package:flutter/material.dart';
import 'package:lexypass/tickets/ticket.dart';

import 'ticket_list_tile.dart';

class TicketList extends StatelessWidget {
  final String _title;
  final List<Ticket> _tickets;

  TicketList(this._title, this._tickets);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
            Align(alignment: Alignment.centerLeft, child: Text(_title)),
            Divider(),
          ] +
          _tickets.map((ticket) => TicketListTile(ticket)).toList(),
    );
  }
}
