import 'package:flutter/material.dart';
import 'package:lexypass/tickets/ticket_product.dart';
import 'package:lexypass/tickets/tickets.dart';
import 'package:lexypass/ui/shared/bottomNavigationBar/bottom_navigation_bar.dart';

import '../../../tickets/ticket.dart';
import 'ticket_list.dart';

class MyPassesPage extends StatefulWidget {
  @override
  State createState() => _MyPassesPageState();
}

class _MyPassesPageState extends State<MyPassesPage> {
  List<Ticket> _tickets;

  @override
  void initState() {
    _fetchTickets();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: _tickets != null
          ? Scaffold(
              appBar: AppBar(
                leading: Container(),
                title: Text("My Passes"),
              ),
              body: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    TicketList("Valid passes", _getValidTickets()),
                    SizedBox(
                      height: 50,
                    ),
                    TicketList("Expired passes", _getExpiredTickets()),
                  ],
                ),
              ),
              bottomNavigationBar: LexyBottomNavigationBar(1),
            )
          : Text("Loading"),
    );
  }

  void _fetchTickets() async {
    Future.delayed(const Duration(milliseconds: 500), () {
      setState(() {
        _tickets = [
          Ticket(TicketType.SINGLE_TICKET, "1 ride", DateTime(2000)),
          Ticket(TicketType.SINGLE_TICKET, "1 ride", DateTime(2020)),
          Ticket(TicketType.TRAVEL_CARD, "10 rides", DateTime(2010)),
          Ticket(TicketType.TRAVEL_CARD, "24 hours", DateTime(2030)),
          Ticket(TicketType.SINGLE_TICKET, "1 ride", DateTime(2000)),
        ];
      });
    });
  }

  List<Ticket> _getValidTickets() =>
      _tickets.where((ticket) => ticket.isValid()).toList();

  List<Ticket> _getExpiredTickets() =>
      _tickets.where((ticket) => !ticket.isValid()).toList();
}
