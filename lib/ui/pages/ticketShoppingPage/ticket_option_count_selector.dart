import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lexypass/tickets/ticket_product.dart';
import 'package:lexypass/ui/pages/ticketShoppingPage//plus_minus_counter.dart';

class TicketOptionCountSelector extends StatelessWidget {
  final TicketProduct _option;
  final int _count;
  final Function(TicketProduct, int) _onCountChanged;

  TicketOptionCountSelector(this._option, this._count, this._onCountChanged);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Column(
          children: <Widget>[
            Text(_option.ticketType.toString()),
            Text(_option.description),
          ],
        ),
        Text(_option.price.value.toString() + " " + _option.price.currency),
        Icon(Icons.close),
        PlusMinusCounter(_count, _notifyAboutNewCount),
      ],
    );
  }

  void _notifyAboutNewCount(int count) {
    _onCountChanged(_option, count);
  }
}
