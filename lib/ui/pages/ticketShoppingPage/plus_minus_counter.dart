import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlusMinusCounter extends StatefulWidget {
  final Function(int) _onCountChanged;
  final int _initialCount;

  PlusMinusCounter(this._initialCount, this._onCountChanged);

  @override
  State createState() => _PlusMinusCounterState();
}

class _PlusMinusCounterState extends State<PlusMinusCounter> {
  int _count;

  @override
  void initState() {
    _count = widget._initialCount;
  }

  @override
  Widget build(BuildContext context) => Container(
        width: 100,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(_count.toString()),
            LimitedBox(
              maxWidth: 30,
              child: OutlineButton(
                color: Colors.red,
                padding: EdgeInsets.all(0),
                child: Icon(Icons.remove),
                onPressed: _decrement,
              ),
            ),
            LimitedBox(
              maxWidth: 30,
              child: OutlineButton(
                padding: EdgeInsets.all(0),
                child: Icon(Icons.add),
                onPressed: _increment,
              ),
            ),
          ],
        ),
      );

  void _decrement() {
    if (_count <= 0) {
      return;
    }
    setState(() {
      _count--;
    });
    widget._onCountChanged(_count);
  }

  void _increment() {
    setState(() {
      _count++;
    });
    widget._onCountChanged(_count);
  }
}
