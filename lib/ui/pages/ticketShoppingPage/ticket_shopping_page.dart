import "package:flutter/material.dart";
import 'package:lexypass/tickets/ticket_product.dart';
import 'package:lexypass/ui/pages/paymentPage/payment_page.dart';
import 'package:lexypass/ui/pages/ticketShoppingPage/ticket_option_count_selector.dart';
import 'package:lexypass/ui/shared/single_button_footer.dart';

class TicketShoppingPage extends StatefulWidget {
  final List<TicketProduct> _options;

  TicketShoppingPage(List<List<TicketProduct>> options)
      : _options = options.expand((i) => i).toList();

  @override
  State createState() => TicketShoppingPageState();
}

class TicketShoppingPageState extends State<TicketShoppingPage> {
  Map<TicketProduct, int> _counts = Map<TicketProduct, int>();

  @override
  void initState() {
    widget._options.forEach((option) => _counts[option] = 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Choose tickets"),
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: ListView(
          children: widget._options
              .map((option) => TicketOptionCountSelector(
                  option, _counts[option], _onCountChanged))
              .toList(),
        ),
      ),
      bottomNavigationBar: _getPayButtonIfReady(context),
    );
  }

  Widget _getPayButtonIfReady(BuildContext context) {
    if (_counts.values.any((value) => value != 0)) {
      return SingleButtonFooter(
        () => Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PaymentPage(_getSelectionsMap()))),
        Text("PAY " + _calculateTotal().toString() + " " + _currency()),
      );
    } else {
      return null;
    }
  }

  double _calculateTotal() => _getSelectionEntries()
      .fold(0, (sum, entry) => sum += entry.key.price.value * entry.value);

  void _onCountChanged(TicketProduct option, int count) {
    setState(() {
      _counts[option] = count;
    });
  }

  Map<TicketProduct, int> _getSelectionsMap() =>
      Map<TicketProduct, int>.fromEntries(_getSelectionEntries());

  Iterable<MapEntry<TicketProduct, int>> _getSelectionEntries() =>
      _counts.entries.where((entry) => entry.value != 0);

  String _currency() => _counts.entries.first.key.price.currency;
}
