import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lexypass/tickets/tickets.dart';
import 'package:lexypass/ui/pages/tripDetailsPage/step_ticket_options.dart';

class RouteTicketOptions extends StatelessWidget {
  final List<List<TicketProduct>> _ticketOptions;

  RouteTicketOptions(this._ticketOptions);

  @override
  Widget build(BuildContext context) => Column(
        children: List<Widget>.from(
            _ticketOptions.map((options) => StepTicketOptions(options))),
      );
}
