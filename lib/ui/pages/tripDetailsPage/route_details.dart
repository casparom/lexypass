import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/directions.dart' as Directions;
import 'package:lexypass/ui/pages/tripDetailsPage/route_step.dart';

class RouteDetails extends StatelessWidget {
  final Directions.Route _route;

  RouteDetails(this._route);

  @override
  Widget build(BuildContext context) {
    List<Directions.Step> steps = _route.legs.first.steps;
    List<Widget> children =
        List<Widget>.from(steps.map((step) => RouteStep(step)).toList());
    children.add(_getDestinationWidget(steps.last));
    return Column(
      children: children,
    );
  }

  Widget _getDestinationWidget(Directions.Step step) {
    return Row(
      children: <Widget>[
        Icon(Icons.location_on),
      ],
    );
  }
}
