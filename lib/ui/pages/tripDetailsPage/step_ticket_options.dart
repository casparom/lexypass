import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:lexypass/tickets/tickets.dart';

class StepTicketOptions extends StatelessWidget {
  final List<TicketProduct> _options;

  StepTicketOptions(this._options);

  @override
  Widget build(BuildContext context) => Padding(
        padding: EdgeInsets.symmetric(vertical: 4.0),
        child: Column(
          children:
              _options.map((option) => _getTicketOptionWidget(option)).toList(),
        ),
      );

  Widget _getTicketOptionWidget(TicketProduct option) {
    return Row(
      children: <Widget>[
        Text(
          describeEnum(option.ticketType),
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Padding(
          padding: EdgeInsets.only(left: 10),
          child: Text(
            option.description,
            style: TextStyle(color: Colors.grey.withOpacity(1.0)),
          ),
        ),
        Spacer(),
        Icon(Icons.backspace),
      ],
    );
  }
}
