import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/directions.dart' as Directions;
import 'package:lexypass/ui/pages/tripDetailsPage/step_details.dart';

class RouteStep extends StatelessWidget {
  final Directions.Step _step;

  RouteStep(this._step);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        _getBulletinWidget(),
        StepDetails(_step),
      ],
    );
  }

  Widget _getBulletinWidget() {
    return Icon(Icons.navigate_next);
  }
}
