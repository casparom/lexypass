import "package:flutter/material.dart";
import 'package:google_maps_webservice/directions.dart' as Directions;
import 'package:google_maps_webservice/directions.dart';
import 'package:lexypass/tickets/ticket_product.dart';
import 'package:lexypass/tickets/tickets.dart';
import 'package:lexypass/ui/pages/ticketShoppingPage/ticket_shopping_page.dart';
import 'package:lexypass/ui/pages/tripDetailsPage/route_details.dart';
import 'package:lexypass/ui/pages/tripDetailsPage/route_map.dart';
import 'package:lexypass/ui/pages/tripDetailsPage/route_ticket_options.dart';
import 'package:lexypass/ui/shared/single_button_footer.dart';

class TripDetailsPage extends StatefulWidget {
  final Directions.Route _route;

  TripDetailsPage(this._route);

  @override
  State createState() => _TripDetailsPageState();
}

class _TripDetailsPageState extends State<TripDetailsPage> {
  List<List<TicketProduct>> _ticketOptions;
  Future<List<List<TicketProduct>>> _ticketOptionFetchingFuture;

  @override
  void initState() {
    _ticketOptionFetchingFuture = _getTicketOptionsForSteps();
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text("Trip details"),
        ),
        body: Padding(
          padding: EdgeInsets.all(20.0),
          child: ListView(
            children: <Widget>[
              RouteDetails(widget._route),
              RouteMap(widget._route),
              FutureBuilder(
                builder: (context, snapshot) {
                  if (snapshot.connectionState != ConnectionState.done) {
                    return Text("Loading ticket options");
                  } else if (!snapshot.hasData || snapshot.hasError) {
                    return Text("No ticket options found");
                  }
                  return RouteTicketOptions(_ticketOptions);
                },
                future: _ticketOptionFetchingFuture,
              )
            ],
          ),
        ),
        bottomNavigationBar:
            _ticketOptions != null ? _getBuyTicketButton(context) : null,
      );

  Widget _getBuyTicketButton(BuildContext context) => SingleButtonFooter(
        () => Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => TicketShoppingPage(_ticketOptions))),
        Text("Buy tickets"),
      );

  Future<List<List<TicketProduct>>> _getTicketOptionsForSteps() async {
    var futures = widget._route.legs.first.steps
        .where((step) => step.travelMode == TravelMode.transit)
        .map((step) => _getTicketOptionsForStep(step.transitDetails));
    return await Future.wait(futures).then((options) {
      onTicketOptionsReceived(options);
      return Future.value(options);
    });
  }

  Future<List<TicketProduct>> _getTicketOptionsForStep(
      TransitDetails transitDetails) async {
    return MerchantFacade().getTicketProducts(transitDetails);
  }

  void onTicketOptionsReceived(List<List<TicketProduct>> options) {
    setState(() {
      _ticketOptions = options;
    });
  }
}
