import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/directions.dart' as Directions;
import 'package:lexypass/util/location_util.dart';
import 'package:lexypass/util/static_map_provider.dart';

class RouteMap extends StatelessWidget {
  final Directions.Route _route;

  RouteMap(this._route);

  @override
  Widget build(BuildContext context) => Image.network(_getStaticMapUri());

  String _getStaticMapUri() => StaticMapProvider()
      .getStaticUriWithMarkersAndZoom(
          markers: List<LatLng>.from([
            locationToLatLng(_route.legs.first.startLocation),
            locationToLatLng(_route.legs.first.endLocation)
          ]),
          pathPoints: _route.overviewPolyline.points)
      .toString();
}
