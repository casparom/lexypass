import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/directions.dart' as Directions;
import 'package:lexypass/util/google_maps_directions_util.dart';

class StepDetails extends StatelessWidget {
  final Directions.Step _step;

  StepDetails(this._step);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                chooseTravelModeIcon(_step),
              ],
            ),
          ],
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(_step.travelMode == Directions.TravelMode.transit
                ? _step.transitDetails.departureStop.name
                : "Walk"),
            Text(_step.travelMode == Directions.TravelMode.transit
                ? _step.transitDetails.headsign
                : "")
          ],
        ),
      ],
    );
  }
}
