import "package:flutter/material.dart";
import 'package:google_maps_webservice/directions.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:lexypass/global/properties.dart';

import 'places_autocomplete_prediction_field.dart';

class GooglePlacesSearchBar extends StatelessWidget {
  final Function(Prediction prediction) _onChanged;
  final Location _location;
  final String hint;

  GooglePlacesSearchBar(this._location, this._onChanged, {this.hint});

  @override
  Widget build(BuildContext context) {
    return PlacesAutocompletePredictionField(
      apiKey: Google.apiKey,
      hint: hint,
      location: _location,
      radius: 1000000,
      onChanged: _onChanged,
    );
  }
}
