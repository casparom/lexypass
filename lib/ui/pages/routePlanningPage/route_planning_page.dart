import "package:flutter/material.dart";
import 'package:google_maps_webservice/directions.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:lexypass/ui/pages/routePlanningPage/google_places_search_widget.dart';
import 'package:lexypass/ui/pages/suggestedTripsPage/suggested_trips_page.dart';
import 'package:lexypass/util/google_places_util.dart';

import 'google_places_search_widget.dart';

class RoutePlanningPage extends StatefulWidget {
  final Location location;

  RoutePlanningPage(this.location);

  @override
  State createState() => _RoutePlanningPageState();
}

class _RoutePlanningPageState extends State<RoutePlanningPage> {
  PlaceDetails _origin;
  PlaceDetails _destination;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Enter your trip"),
      ),
      body: Column(
        children: <Widget>[
          GooglePlacesSearchBar(widget.location, _onOriginSelected,
              hint: "From"),
          GooglePlacesSearchBar(widget.location, _onDestinationSelected,
              hint: "To"),
        ],
      ),
    );
  }

  void _onOriginSelected(Prediction prediction) async {
    _origin = await getPlaceDetailsByPlaceId(prediction.placeId);
    _showRoutesIfReady();
  }

  void _onDestinationSelected(Prediction prediction) async {
    _destination = await getPlaceDetailsByPlaceId(prediction.placeId);
    _showRoutesIfReady();
  }

  void _showRoutesIfReady() {
    if (_origin != null && _destination != null) {
      MaterialPageRoute route = MaterialPageRoute(
          builder: (context) => SuggestedTripsPage(_origin, _destination));
      Navigator.push(context, route);
    }
  }
}
