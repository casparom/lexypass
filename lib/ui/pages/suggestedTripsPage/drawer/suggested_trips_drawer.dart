import "package:flutter/material.dart";
import 'package:google_maps_webservice/directions.dart' as Directions;
import 'package:google_maps_webservice/places.dart';
import 'package:lexypass/util/google_maps_directions_util.dart';

import 'trip_suggestion_tile.dart';

class SuggestedTripsDrawer extends StatefulWidget {
  final PlaceDetails _origin;
  final PlaceDetails _destination;
  final Function(Directions.Route) _onRouteSelected;

  SuggestedTripsDrawer(this._origin, this._destination, this._onRouteSelected);

  @override
  _SuggestedTripsDrawerState createState() {
    return new _SuggestedTripsDrawerState();
  }
}

class _SuggestedTripsDrawerState extends State<SuggestedTripsDrawer> {
  Directions.DirectionsResponse _directions;

  @override
  void initState() {
    _fetchDirections();
  }

  @override
  Widget build(BuildContext context) {
    if (_directions == null) {
      return Align(child: Text("Loading routes"));
    }

    List<TripSuggestionTile> tiles =
        _directions.routes.map((data) => _buildTripTile(data)).toList();

    List<TripSuggestionTile> previewTiles =
        List<TripSuggestionTile>.from(tiles);
    if (previewTiles.length > 2) {
      previewTiles.length = 2;
    }

    return BottomAppBar(
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.maximize),
              onPressed: () {
                _showDrawer(tiles);
              },
            ),
            ...previewTiles,
          ],
        ),
      ),
    );
  }

  _showDrawer(List<TripSuggestionTile> tiles) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            child: ListView(
              children: [
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: tiles,
                )
              ],
            ),
          );
        });
  }

  void _fetchDirections() {
    getDirections(widget._origin, widget._destination).then(_setDirections);
  }

  void _setDirections(Directions.DirectionsResponse directions) {
    setState(() {
      _directions = directions;
    });
  }

  TripSuggestionTile _buildTripTile(Directions.Route route) =>
      TripSuggestionTile(route, () {
        widget._onRouteSelected(route);
      });
}
