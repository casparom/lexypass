import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/directions.dart' as Directions;
import 'package:lexypass/util/google_maps_directions_util.dart';

class TripSuggestionTile extends StatelessWidget {
  final Directions.Route _route;
  final Function _onTap;

  TripSuggestionTile(this._route, this._onTap);

  @override
  Widget build(BuildContext context) {
    Directions.Leg leg = _route.legs.first;

    return ListTile(
      onTap: _onTap,
      title: _buildStepsWidget(leg),
      subtitle: _buildDistanceAndPriceWidget(leg),
    );
  }

  Widget _buildStepsWidget(Directions.Leg leg) {
    final List<Widget> steps = List<Widget>();
    leg.steps.forEach((step) {
      steps.add(chooseTravelModeIcon(step));
      steps.add(Icon(Icons.keyboard_arrow_right));
    });
    steps.removeLast();
    steps.add(Spacer());
    steps.add(_buildEstimatedTimeWidget(leg));
    return Row(
      children: steps,
    );
  }

  Widget _buildDistanceAndPriceWidget(Directions.Leg leg) {
    return Row(
      children: <Widget>[
        Icon(Icons.location_on),
        Text(leg.distance.text),
        Icon(Icons.attach_money),
        Text("123 eur"),
      ],
    );
  }

  Widget _buildEstimatedTimeWidget(Directions.Leg leg) {
    return Row(
      children: <Widget>[
        Text(leg.duration.text),
        Icon(Icons.keyboard_arrow_right),
      ],
    );
  }
}
