import "package:flutter/material.dart";
import "package:google_maps_flutter/google_maps_flutter.dart";
import 'package:google_maps_webservice/directions.dart' as Directions;
import 'package:google_maps_webservice/places.dart';
import 'package:lexypass/global/properties.dart';
import 'package:lexypass/ui/pages/tripDetailsPage/trip_details_page.dart';
import 'package:lexypass/ui/shared/map_widget.dart';
import 'package:lexypass/util/google_places_util.dart';
import 'package:lexypass/util/location_util.dart';

import 'drawer/suggested_trips_drawer.dart';

class SuggestedTripsPage extends StatelessWidget {
  final PlaceDetails _origin;
  final PlaceDetails _destination;

  SuggestedTripsPage(this._origin, this._destination);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Suggested trips"),
      ),
      body: Column(
        children: <Widget>[
          MapWidget(
            onMapCreated: _onMapCreated,
            markers: Set<Marker>.from([
              _createMarker(getLatLngByPlaceDetails(_origin)),
              _createMarker(getLatLngByPlaceDetails(_destination))
            ]),
          ),
        ],
      ),
      bottomNavigationBar: SuggestedTripsDrawer(_origin, _destination,
          (Directions.Route route) => _onRouteSelected(context, route)),
    );
  }

  Marker _createMarker(LatLng latLng) => Marker(
        markerId: MarkerId(latLng.toString()),
        position: latLng,
      );

  void _onMapCreated(GoogleMapController controller) async {
    Future.delayed(Duration(seconds: 1), () {
      Tuple2 corners = calculateSouthWestAndNorthEast(
          getLatLngByPlaceDetails(_origin),
          getLatLngByPlaceDetails(_destination));
      controller.animateCamera(
        CameraUpdate.newLatLngBounds(
          LatLngBounds(
            southwest: corners.item1,
            northeast: corners.item2,
          ),
          MapView.mapBoundViewPadding,
        ),
      );
    });
  }

  void _onRouteSelected(BuildContext context, Directions.Route route) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => TripDetailsPage(route),
      ),
    );
  }
}
