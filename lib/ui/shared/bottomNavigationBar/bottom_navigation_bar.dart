import "package:flutter/material.dart";
import 'package:lexypass/ui/pages/myPassesPage/my_passes_page.dart';
import 'package:lexypass/ui/pages/startPage/start_page.dart';
import 'package:lexypass/ui/shared/addCreditCardDialog/add_credit_card_dialog.dart';

class LexyBottomNavigationBar extends StatefulWidget {
  final int _initialIndex;

  LexyBottomNavigationBar(this._initialIndex);

  @override
  State createState() => _LexyBottomNavigationBarState();
}

class _LexyBottomNavigationBarState extends State<LexyBottomNavigationBar> {
  int _selectedIndex = 0;

  @override
  void initState() {
    _selectedIndex = widget._initialIndex;
    super.initState();
  }

  @override
  Widget build(BuildContext context) => BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.near_me),
            title: Text('Planner'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.bookmark),
            title: Text('My Passes'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            title: Text('My Account'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.red,
        onTap: (index) => _onItemTapped(index, context),
      );

  void _onItemTapped(int index, BuildContext context) {
    if (index != _selectedIndex) {
      setState(() {
        _selectedIndex = index;
      });
      _navigateToIndexPage(context);
    }
  }

  void _navigateToIndexPage(BuildContext context) {
    switch (_selectedIndex) {
      case 0:
        _navigateTo(StartPage(), context);
        break;
      case 1:
        _navigateTo(MyPassesPage(), context);
        break;
      case 2:
        AddCreditCardDialog.show(context);
        break;
    }
  }

  void _navigateTo(Widget widget, BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => widget));
  }
}
