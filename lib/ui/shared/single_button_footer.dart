import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SingleButtonFooter extends StatelessWidget {
  final Function() _onPressed;
  final Widget _child;

  SingleButtonFooter(this._onPressed, this._child);

  @override
  Widget build(BuildContext context) => Container(
        height: 70,
        child: Align(
          child: Padding(
            padding: EdgeInsets.all(15),
            child: SizedBox.expand(
              child: RaisedButton(
                onPressed: _onPressed,
                child: _child,
              ),
            ),
          ),
        ),
      );
}
