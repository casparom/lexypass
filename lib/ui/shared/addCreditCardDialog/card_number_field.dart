import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CardNumberField extends StatelessWidget {
  final Function(String) _onSaved;

  CardNumberField(this._onSaved);

  @override
  Widget build(BuildContext context) => TextFormField(
        decoration: InputDecoration(labelText: "Card number"),
        validator: (value) {
          if (!RegExp(
                  "^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})\$")
              .hasMatch(value)) {
            return "Please enter a valid credit card number";
          }
          return null;
        },
        keyboardType: TextInputType.number,
        inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
        onSaved: _onSaved,
      );
}
