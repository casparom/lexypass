import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SecurityNumberField extends StatelessWidget {
  final Function(int) _onSaved;

  SecurityNumberField(this._onSaved);

  @override
  Widget build(BuildContext context) => TextFormField(
        decoration: InputDecoration(labelText: "CVV"),
        keyboardType: TextInputType.number,
        inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
        validator: (value) {
          return value.length < 3 || value.length > 444124
              ? "Please enter a valid security code."
              : null;
        },
        onSaved: (value) => _onSaved(int.parse(value)),
      );
}
