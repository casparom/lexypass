import 'package:flutter/material.dart';
import 'package:lexypass/payment/methods/credit_card.dart';
import 'package:lexypass/payment/payment_method_facade.dart';

import 'card_number_field.dart';
import 'security_number_field.dart';
import 'valid_until_field.dart';

class AddCreditCardDialog extends StatefulWidget {
  static show(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) => AddCreditCardDialog());

  @override
  State createState() => _AddCreditCardDialogState();
}

class _AddCreditCardDialogState extends State<AddCreditCardDialog> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _cardNumber;
  DateTime _validUntil;
  int _securityNumber;

  @override
  Widget build(BuildContext context) => AlertDialog(
        title: Text("Add credit card"),
        content: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              CardNumberField((value) {
                _cardNumber = value;
              }),
              Row(
                children: <Widget>[
                  Expanded(
                    child: ValidUntilField((value) {
                      _validUntil = value;
                    }),
                  ),
                  SizedBox(
                    width: 25,
                  ),
                  Expanded(
                    child: SecurityNumberField(
                      (value) {
                        _securityNumber = value;
                      },
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: RaisedButton(
                      onPressed: _onSubmit,
                      child: Text('Submit'),
                    ),
                  ),
                  Spacer(),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: RaisedButton(
                      onPressed: _onCancel,
                      child: Text('Cancel'),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      );

  void _onSubmit() {
    FormState state = _formKey.currentState;
    if (state.validate()) {
      state.save();
      CreditCard card = CreditCard(_cardNumber, _validUntil, _securityNumber);
      PaymentMethodFacade().addCreditCard(card);
      Navigator.pop(context, card);
    }
  }

  void _onCancel() {
    Navigator.pop(context, null);
  }
}
