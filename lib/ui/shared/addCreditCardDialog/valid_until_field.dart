import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ValidUntilField extends StatelessWidget {
  final Function(DateTime) _onSaved;

  ValidUntilField(this._onSaved);

  @override
  Widget build(BuildContext context) => DateTimeField(
        decoration: InputDecoration(labelText: "Valid until"),
        format: DateFormat("yyyy-MM"),
        onShowPicker: (context, currentValue) {
          return showDatePicker(
              context: context,
              firstDate: DateTime.now(),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(2100));
        },
        validator: (value) {
          return value == null ? "Please fill." : null;
        },
        onSaved: _onSaved,
      );
}
