import 'package:flutter/material.dart';

void showNotSupportedDialog(BuildContext context) => showDialog(
    context: context,
    builder: (BuildContext context) =>
        AlertDialog(title: Text("Not supported yet")));
