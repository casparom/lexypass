import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapWidget extends StatelessWidget {
  final MapCreatedCallback onMapCreated;
  final Set<Marker> markers;

  MapWidget({this.onMapCreated, this.markers});

  @override
  Widget build(BuildContext context) => Expanded(
        child: GoogleMap(
          myLocationEnabled: true,
          myLocationButtonEnabled: true,
          initialCameraPosition:
              CameraPosition(target: LatLng(59.334591, 18.063240), zoom: 14),
          onMapCreated: this.onMapCreated,
          markers: markers,
        ),
      );
}
