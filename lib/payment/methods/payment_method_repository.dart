import 'package:lexypass/payment/methods/credit_card.dart';

import 'payment_method.dart';

abstract class PaymentMethodRepository {
  Future<List<PaymentMethod>> getAll();
  void saveCreditCard(CreditCard card);
}
