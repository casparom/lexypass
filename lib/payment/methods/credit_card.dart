import 'payment_method.dart';

class CreditCard extends PaymentMethod {
  static final _idColumn = "id";
  static final _validUntilColumn = "valid_until";
  static final _securityNumberColumn = "security_number";

  final String _number;
  final DateTime _validUntil;
  final int _securityNumber;

  CreditCard(this._number, this._validUntil, this._securityNumber)
      : super(_number);

  String name() {
    var length = _number.length;
    return "**** " + _number.substring(length - 4, length);
  }

  Map<String, dynamic> toMap() => {
        _idColumn: _number,
        _validUntilColumn: _validUntil.toIso8601String(),
        _securityNumberColumn: _securityNumber,
      };

  static CreditCard fromMap(Map<String, dynamic> map) => CreditCard(
      map[_idColumn],
      DateTime.parse(map[_validUntilColumn]),
      map[_securityNumberColumn]);
}
