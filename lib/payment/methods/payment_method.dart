abstract class PaymentMethod {
  final String _id;

  PaymentMethod(this._id);

  String id() => _id;
  String name();

  bool operator ==(dynamic other) {
    if (other.runtimeType != this.runtimeType) {
      return false;
    }
    return id() == (other as PaymentMethod).id();
  }

  @override
  int get hashCode => _id.hashCode;
}
