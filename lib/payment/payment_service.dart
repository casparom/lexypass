import 'package:lexypass/payment/methods/payment_method.dart';
import 'package:lexypass/tickets/tickets.dart';

class PaymentServiceFacade {
  static final PaymentServiceFacade _instance =
      PaymentServiceFacade._privateConstructor();

  factory PaymentServiceFacade() => _instance;

  PaymentServiceFacade._privateConstructor();

  Future<bool> pay(Map<TicketProduct, int> produts, PaymentMethod method) =>
      Future.delayed(Duration(seconds: 2), () => Future.value(true));
}
