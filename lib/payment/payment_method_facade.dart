import 'package:lexypass/payment/methods/credit_card.dart';
import 'package:lexypass/payment/methods/payment_method.dart';

import 'methods/payment_method_repository.dart';

class PaymentMethodFacade {
  static final PaymentMethodFacade _instance =
      PaymentMethodFacade._privateConstructor();

  factory PaymentMethodFacade() => _instance;

  PaymentMethodFacade._privateConstructor();

  PaymentMethodRepository _repository;

  void setRepository(PaymentMethodRepository repository) {
    _repository = repository;
  }

  Future<List<PaymentMethod>> getMethods() => _repository.getAll();

  void addCreditCard(CreditCard card) => _repository.saveCreditCard(card);
}
