import 'package:lexypass/localStorage/database.dart';

class LocalDatabaseDummy implements LocalDatabase {
  Future<void> close() => Future.value();

  Future<List<Map<String, dynamic>>> read(String table) =>
      Future.value(List<Map<String, dynamic>>());

  void insert(String table, Map<String, dynamic> values) {}
}
