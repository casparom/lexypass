
import 'package:lexypass/localStorage/database.dart';
import 'package:lexypass/localStorage/database_factory.dart';

import 'local_database_dummy.dart';

class LocalDatabaseFactoryDummy implements LocalDatabaseFactory {
  @override
  Future<LocalDatabase> create() => Future.value(LocalDatabaseDummy());
}
